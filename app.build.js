({
  appDir: 'public',     // Where the HTML and JS is stored.
  baseUrl: 'js',        // Top-level directory containing the JS.
  name: 'main',        // Filename containing the top-level requirejs application.
  dir: 'dist',           // Destination of the optimized version of the application.
  optimizeCss: 'standard',
  mainConfigFile: 'public/js/main.js',
  findNestedDependencies: true,
  paths: {
    socketio: 'vendor/socket.io',
    peerjs: 'vendor/peer.min',
    underscore: 'vendor/underscore.min',
    domReady: 'vendor/domReady'
  },
  shim: {
    socketio: {
      exports: 'io'
    },
    peerjs: {
      exports: 'peerjs'
    },
    underscore: {
      exports: '_'
    },
    domReady: {
      exports: 'domReady'
    }
  }
})
