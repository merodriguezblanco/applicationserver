define('TorrentParser', ['Torrent'], function (Torrent) {

  'use strict';

  var trackers = function () {
    return JSON.parse(Torrent).trackers;
  }

  var videoMetadata = function () {
    return JSON.parse(Torrent).videoMetadata;
  }

  return {
    parseTrackers: trackers,
    parseVideoMetadata: videoMetadata
  }

});
