define('MediaChunk', function () {

  'use strict';

  var MediaChunk = function(data) {

    this.buffered = false;
    this.requested = false;
    this.chunk = undefined;
    this.offset = data.offset;
    this.size = data.size;
    this.time = data.timecode;

    var self = this;

    var markAsBuffered = function() {
      self.buffered = true;
    }

    var markAsRequested = function() {
      self.requested = true;
    }

    var markAsNotRequested = function() {
      self.requested = false;
    }

    var setChunk = function(data) {
      self.chunk = data;
    }

    var getChunk = function() {
      return self.chunk;
    }

    var getOffset = function() {
      return self.offset;
    }

    var getSize = function() {
      return self.size;
    }

    var getTime = function() {
      return self.time;
    }

    var isRequested = function() {
      return self.requested;
    }

    var isLoaded = function() {
      return !_.isUndefined(self.chunk);
    }

    var isRequestable = function() {
      return (!isLoaded() && !isRequested());
    }

    return {
      markAsBuffered: markAsBuffered,
      markAsRequested: markAsRequested,
      markAsNotRequested: markAsNotRequested,
      setChunk: setChunk,

      getChunk: getChunk,
      getOffset: getOffset,
      getSize: getSize,
      getTime: getTime,

      isRequested: isRequested,
      isLoaded: isLoaded,
      isRequestable: isRequestable
    }

  }

  return MediaChunk;

});