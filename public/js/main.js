requirejs.config({
  baseUrl: '/js',
  paths: {
    socketio: '/js/vendor/socket.io',
    peerjs: '/js/vendor/peer.min',
    underscore: '/js/vendor/underscore.min',
    domReady: '/js/vendor/domReady'
  },
  shim: {
    socketio: {
      exports: 'io'
    },
    peerjs: {
      exports: 'peerjs'
    },
    underscore: {
      exports: '_'
    },
    domReady: {
      exports: 'domReady'
    }
  },
});

var globalEventListener = (function () {
  return this;
}());

require(['underscore', 'peerjs', 'socketio', 'domReady'], function (_, peerjs, io, domReady) {
  require(['Tracker', 'Broker', 'Receiver', 'Front', 'Utils'],
          function (Tracker, Broker, Receiver, Front, Utils) {

    var VIDEO_FILE_NAME = 'remuxed_2sec.webm';

    var initialize = function (event) {
      Broker.initialize(VIDEO_FILE_NAME, event.data);
      Receiver.initialize();
    }

    domReady(function () {
      globalEventListener.addEventListener('peerInitialized', initialize);
      globalEventListener.addEventListener('mediaSourceOpen', function () { Broker.start() });

      Front.initialize();
      Tracker.initHostPeer();
    });
  });
});
