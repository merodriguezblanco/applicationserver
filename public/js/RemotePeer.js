define('RemotePeer', ['Front', 'MediaSourceProxy', 'ChunkStreamer', 'ChunkSerializer', 'RarestChunkHelper'],
       function (Front, MediaSourceProxy, ChunkStreamer, ChunkSerializer, RarestChunkHelper) {

  'use strict';

  var DC_REQUEST_CHUNK        = 'dc-request-chunk',
      DC_RESPONSE_CHUNK       = 'dc-response-piece',
      DC_RESPONSE_NULL_CHUNK  = 'dc-response-null-chunk',
      HANDSHAKE_RESPONSE      = 'handshake-response',
      HANDSHAKE_ESTABLISHMENT = 'handshake-establishment',
      HAVE                    = 'have',
      KEEP_ALIVE              = 'keep-alive',
      CHOKE                   = 'choke',
      UNCHOKE                 = 'unchoke',
      INTERESTED              = 'interested',
      NOT_INTERESTED          = 'not-interested';

  var UNCHOKED_LIMIT     = 4,
      STALE_TIME_WINDOW  = 20000,
      BYTE_RATES_LIMIT   = 3,
      INITIAL_BYTE_RATE  = 0,
      BYTE_RATE_COEF_1_3 = 0.2,
      BYTE_RATE_COEF_2_3 = 0.3,
      BYTE_RATE_COEF_3_3 = 0.5,
      BYTE_RATE_COEF_1_2 = 0.4,
      BYTE_RATE_COEF_2_2 = 0.6;

  var RemotePeer = function (id) {

    var self = this;

    self.id                  = id;
    self.requesting          = false;
    self.executionChunkNumber;
    self.bitfield;
    self.handshakeEstablished = false;
    self.lastSentTime        = new Date();
    self.lastReceivedTime    = new Date();
    self.byteRates           = [INITIAL_BYTE_RATE];

    self.remoteChoked     = true;
    self.remoteInterested = false;
    self.localChoked      = true;
    self.localInterested  = false;

    self.chunksDelivered = 0;
    self.chunksDeliveredReported = 0;

    self.isRequesting = function() {
      return self.requesting;
    }

    //
    //
    // ACTION INTERPRETER

    self.onDataReceived = function (data) {
      var msg = JSON.parse(data);

      if (msg.type === DC_REQUEST_CHUNK) {
        onChunkRequest(msg);
      } else if (msg.type === DC_RESPONSE_CHUNK) {
        onPieceResponse(msg);
      } else if (msg.type === DC_RESPONSE_NULL_CHUNK) {
        onNullChunkResponse(msg);
      } else if (msg.type === HANDSHAKE_RESPONSE) {
        onHandshakeResponse(msg);
      } else if (msg.type === HANDSHAKE_ESTABLISHMENT) {
        onHandshakeEstablished(msg);
      } else if (msg.type === HAVE) {
        onReceiveHave(msg);
      } else if (msg.type === KEEP_ALIVE) {
        onReceiveKeepAlive(msg);
      } else if (msg.type === CHOKE) {
        onReceiveChoke(msg);
      } else if (msg.type === UNCHOKE) {
        onReceiveUnchoke(msg);
      } else if (msg.type === INTERESTED) {
        onReceiveInterested(msg);
      } else if (msg.type === NOT_INTERESTED) {
        onReceiveNotInterested(msg);
      } else {
        logActivity(msg.type, 'malformed data');
      }

      self.lastReceivedTime  = new Date();
    }

    //
    //
    // SIGNALING AND CONNECTIONS

    self.connect = function () {
      logActivity('connect', 'request to');
      self.peerJs.connection = self.peerJs.connect(
        self.id,
        {
          metadata: {
            bitfield: MediaSourceProxy.getBitfield(),
            executionChunkNumber: MediaSourceProxy.getExecutionChunkNumber()
          }
        }
      );
      self.peerJs.connection.on('open', onConnectionOpened);
    }

    self.tryConnect = function () {
      if (!self.handshakeEstablished) {
        self.connect();
      }
    }

    var onConnectionOpened = function () {
      var connection = retrieveConnection();
      connection.on('data',  self.onDataReceived);
      connection.on('error', self.onError);
    }

    // Returns the NEWEST active DataConnection between remotePeer and hostPeer or undefined otherwise.
    var retrieveConnection = function () {
      var connections = self.peerJs.connections[self.id].slice();
      return _.findWhere(connections.reverse(), { open: true });
    }

    self.sendHave = function (chunkNumber) {
      if (chunkNumber > self.executionChunkNumber &&
          self.bitfield &&
          self.bitfield[chunkNumber] === 0) {
        send({
          type: HAVE,
          number: chunkNumber
        });
      }
    }

    var sendHandshakeResponse = function () {
      var bitfield = MediaSourceProxy.getBitfield();
      var executionChunkNumber = MediaSourceProxy.getExecutionChunkNumber();
      send({
        type: HANDSHAKE_RESPONSE,
        bitfield: bitfield,
        executionChunkNumber: executionChunkNumber
      });
    }

    var sendHandshakeEstablished = function () {
      send({ type: HANDSHAKE_ESTABLISHMENT });
    }

    var onHandshakeResponse = function (data) {
      if (MediaSourceProxy.hasNeededChunks(data.bitfield)) {
        logActivity('handshake', 'established');
        self.bitfield = data.bitfield;
        self.executionChunkNumber = data.executionChunkNumber;
        self.handshakeEstablished = true;
        sendHandshakeEstablished();

        assumeMutualInterestAndTryUnchoke();
      }
    }

    var onHandshakeEstablished = function (data) {
      logActivity('handshake', 'established');
      self.handshakeEstablished = true;
      assumeMutualInterestAndTryUnchoke();
    }

    self.onConnectionResponse = function (bitfield, executionChunkNumber) {
      logActivity('connect', 'receive from');
      self.bitfield = bitfield;
      self.executionChunkNumber = executionChunkNumber;
      sendHandshakeResponse();
    }

    var onReceiveHave = function (data) {
      if (self.bitfield) {
        self.bitfield[data.number] = 1;
        evaluateInterest();
      }
    }

    var onReceiveKeepAlive = function (data) {
      self.lastReceivedTime = new Date();
      self.executionChunkNumber = data.executionChunkNumber;
    }

    self.onError = function (error) {
      logActivity(error, 'error');
    }

    // CHOKE/UNCHOKE

    var choke = function () {
      setTimeout(Front.paintPeerAsChoked(id, 'local'), 0);
      self.localChoked = true;
      send({ type: CHOKE });
    }

    self.tryChoke = function () {
      if (self.localChoked === false) { choke(); }
    }

    var onReceiveChoke = function(data) {
      setTimeout(Front.paintPeerAsChoked(id, 'remote'), 0);
      self.remoteChoked = true;
    }

    self.unchoke = function () {
      setTimeout(Front.paintPeerAsUnchoked(id, 'local'), 0);
      self.localChoked = false;
      send({ type: UNCHOKE });
    }

    self.tryUnchoke = function () {
      if (self.localInterested === true &&
          self.localChoked === true &&
          RemotePeer.localUnchoked().length < UNCHOKED_LIMIT) {
       self.unchoke();
      }
    }

    var onReceiveUnchoke = function (data) {
      setTimeout(Front.paintPeerAsUnchoked(id, 'remote'), 0);
      self.remoteChoked = false;
    }

    // INTERESTED

    self.interested = function () {
      self.localInterested = true;
      send({ type: INTERESTED });
    }

    var onReceiveInterested = function (data) {
      self.remoteInterested = true;
      self.tryUnchoke();
    }

    var tryInterested = function () {
      if (self.localInterested === false) { self.interested(); }
    }

    var tryInterestedAndUnchoke = function () {
      tryInterested();
      self.tryUnchoke();
    }

    var assumeMutualInterestAndTryUnchoke = function () {
      self.localInterested  = true;
      self.remoteInterested = true;
      self.tryUnchoke();
    }

    var evaluateInterest = function () {
      if (self.bitfield) {
        var interested = MediaSourceProxy.hasNeededChunks(self.bitfield);
        if (interested) { tryInterestedAndUnchoke(); }
      }
    }

    self.evaluateUninterest = function (chunkNumber) {
      if (self.bitfield) {
        var stillInterested = MediaSourceProxy.hasNeededChunks(self.bitfield, chunkNumber);
        if (!stillInterested) {
          self.tryNotInterestedAndChoke();
        }
      }
    }

    var notInterested = function () {
      self.localInterested = false;
      send({ type: NOT_INTERESTED });
    }

    var onReceiveNotInterested = function (data) {
      self.remoteInterested = false;
      self.tryChoke();
    }

    var tryNotInterested = function () {
      if (self.localInterested === true) { notInterested(); }
    }

    self.tryNotInterestedAndChoke = function () {
      tryNotInterested();
      self.tryChoke();
    }

    //
    //
    // CHUNK TRANSACTIONS

    self.requestChunk = function (chunkNumber) {
      var request = {
        type: DC_REQUEST_CHUNK,
        chunkNumber: chunkNumber
      };

      if (send(request)) {
        // TODO: Should we trigger a documentOnChunkRequested event so a timer starts here?
        logActivity(chunkNumber, 'request to');
        self.requesting = true;
      } else {
        logActivity('disconnected', 'request to');
        notifyOnChunkEmpty({ chunkNumber: chunkNumber, binary: null, resourceId: self.id, resourceType: 'peer' });
      }
    }

    var onChunkRequest = function (request) {
      var chunkNumber = request.chunkNumber,
          chunk       = MediaSourceProxy.getChunk(chunkNumber);
      logActivity(chunkNumber, 'request from');

      if (!self.localChoked && chunk) {
        var streamer = new ChunkStreamer(chunk);
        streamer.stream(function() {
          send({
            type: DC_RESPONSE_CHUNK,
            chunkNumber: chunkNumber,
            totalPieces: streamer.totalPieces,
            pieceData: {
              number: streamer.currentPieceNumber,
              piece: streamer.currentPiece
            }
          });
        });
      } else {
        send({
          type: DC_RESPONSE_NULL_CHUNK,
          chunkNumber: chunkNumber
        });
      }
    }

    var onPieceResponse = function (response) {
      var chunkNumber = response.chunkNumber,
          totalPieces = response.totalPieces,
          piece       = response.pieceData.piece,
          pieceNumber = response.pieceData.number,
          connection  = retrieveConnection();

      var serializer = ChunkSerializer.prototype.instance(chunkNumber, connection.peer, totalPieces);
      serializer.appendPiece(pieceNumber, piece);

      if (serializer.chunkIsFull()) {
        onChunkReceived(chunkNumber, serializer.getChunk());
        ChunkSerializer.prototype.forgetInstance(serializer);
      }
    }

    var onChunkReceived = function(chunkNumber, chunk) {
      logActivity(chunkNumber, 'receive from');
      notifyOnChunkReceived({ chunkNumber: chunkNumber, binary: chunk, resourceId: self.id, resourceType: 'peer' });
    }

    var onNullChunkResponse = function (response) {
      var chunkNumber = response.chunkNumber;
      logActivity(chunkNumber + ' empty', 'receive from');

      notifyOnChunkEmpty({ chunkNumber: chunkNumber, data: null, resourceId: self.id });
    }

    var notifyOnChunkReceived = function (data) {
      self.requesting = false;
      var event = new CustomEvent('documentOnChunkReceived');
      event.data = data;
      globalEventListener.dispatchEvent(event);
    }

    var notifyOnChunkEmpty = function (data) {
      self.requesting = false;
      var event = new CustomEvent('documentOnChunkEmpty');
      event.data = data;
      globalEventListener.dispatchEvent(event);
    }

    // Triggered when the chunk is loaded in the MediaSourceProxy
    self.updateStatsOnChunkLoaded = function (chunk) {
      self.updateLastByteRate(chunk.length);
      chunkDelivered();
    }

    //
    //
    // STATS

    self.buildStats = function () {
      return {
        type: 'peer',
        numbers: {
          chunksDelivered: getChunksDeliveredSinceLastReport()
        }
      }
    }

    var getChunksDeliveredSinceLastReport = function () {
      var difference = self.chunksDelivered - self.chunksDeliveredReported;
      self.chunksDeliveredReported = self.chunksDelivered;
      return difference;
    }

    var chunkDelivered = function () {
      self.chunksDelivered++;
    }

    //
    //
    // UTILITIES

    var updateLastSentTime = function () {
      self.updateLastSentTime = new Date();
    }

    self.updateLastByteRate = function (bytes) {
      self.byteRates[self.byteRates.length - 1] += bytes;
      setTimeout(Front.updateStatistic(id, parseFloat(self.averageByteRate() / 1024).toFixed(0) + 'KB'), 0);
    }

    self.shiftByteRates = function () {
      var byteRates = (self.byteRates.length === BYTE_RATES_LIMIT) ? _.tail(self.byteRates) : self.byteRates;
      self.byteRates = byteRates.concat([INITIAL_BYTE_RATE]);
    }

    self.averageByteRate = function() {
      if (self.byteRates.length === BYTE_RATES_LIMIT) {
        return self.byteRates[0] * BYTE_RATE_COEF_1_3 +
               self.byteRates[1] * BYTE_RATE_COEF_2_3 +
               self.byteRates[2] * BYTE_RATE_COEF_3_3;
      } else if (self.byteRates.length === 2) {
        return self.byteRates[0] * BYTE_RATE_COEF_1_2 +
               self.byteRates[1] * BYTE_RATE_COEF_2_2;
      } else if (self.byteRates.length === 1) {
        return self.byteRates[0];
      }
    }

    self.isActive = function () {
      var currentDate = new Date();
      var isPeerStale = (currentDate.getTime() - self.lastReceivedTime.getTime() > STALE_TIME_WINDOW)
      return (!isPeerStale && (!self.handshakeEstablished || self.localInterested));
    }

    self.sendKeepAlive = function (executionChunkNumber) {
      if (self.handshakeEstablished) {
        send({
          type: KEEP_ALIVE,
          executionChunkNumber: executionChunkNumber
        });
      }
    }

    self.getId = function () {
      return self.id;
    }

    var send = function (message) {
      var connection = retrieveConnection();

      if (!_.isUndefined(connection)) {
        connection.send(JSON.stringify(message));
        updateLastSentTime();
        return true;
      } else {
        return false;
      }
    }

    var logActivity = function (upText, downText) {
      setTimeout(Front.renderActivity({ topic: id, data: { up: upText, down: downText } }), 0);
    }

  }

  //
  //
  //CLASS METHODS

  RemotePeer.remotePeers = [];

  RemotePeer.findOrCreate = function (peerId) {
    var remotePeer = RemotePeer.find(peerId);
    if (!remotePeer) {
      remotePeer = new RemotePeer(peerId);
      RemotePeer.remotePeers.push(remotePeer);
    }
    return remotePeer;
  }

  RemotePeer.find = function (peerId) {
    return _.findWhere(RemotePeer.all(), { id: peerId });
  }

  RemotePeer.create = function (swarmRemotePeers) {
    return _.map(swarmRemotePeers, function (swarmRemotePeer) { return RemotePeer.findOrCreate(swarmRemotePeer.id) });
  }

  RemotePeer.connect = function () {
    _.each(RemotePeer.all(), function (peer) { peer.connect() });
  }

  RemotePeer.tryConnect = function (peers) {
    _.each(peers, function (peer) { peer.tryConnect() });
  }

  RemotePeer.sendKeepAlive = function (executionChunkNumber) {
    _.each(RemotePeer.all(), function (peer) { peer.sendKeepAlive(executionChunkNumber) });
  }

  RemotePeer.sendHave = function (chunkNumber) {
    _.each(RemotePeer.all(), function (peer) { peer.sendHave(chunkNumber) });
  }

  RemotePeer.tryChoke = function (peers) {
    _.each(peers, function (peer) { peer.tryChoke() });
  }

  RemotePeer.tryUnchoke = function (peers) {
    _.each(peers, function (peer) { peer.tryUnchoke() });
  }

  RemotePeer.purge = function () {
    RemotePeer.paintAsInactive(RemotePeer.inactive());
    RemotePeer.remotePeers = RemotePeer.active();
  }

  RemotePeer.shiftByteRates = function () {
    _.each(RemotePeer.all(), function (peer) { peer.shiftByteRates() });
  }

  RemotePeer.evaluateUninterest = function (chunkNumber) {
    var interestedPeers = RemotePeer.localInterested();

    _.each(interestedPeers, function(peer) { peer.evaluateUninterest(chunkNumber) });
  }

  RemotePeer.buildStats = function () {
    var stats = {};
    _.each(RemotePeer.all(), function (peer) {
      stats[peer.getId()] = peer.buildStats();
    });
    return stats;
  }

  RemotePeer.paintAsInactive = function (peers) {
    _.each(peers, function (peer) { setTimeout(Front.paintPeerAsInactive(peer.id), 0); });
  }

  // scopes

  RemotePeer.all = function() {
    return RemotePeer.remotePeers;
  }

  RemotePeer.handshakeEstablished = function (peers) {
    return _.select(peers || RemotePeer.all(), function(peer) { return peer.handshakeEstablished === true });
  }

  RemotePeer.localUnchoked = function (peers) {
    return _.select(peers || RemotePeer.all(), function(peer) { return peer.localChoked === false });
  }

  RemotePeer.localChoked = function (peers) {
    return _.select(peers || RemotePeer.all(), function(peer) { return peer.localChoked === true });
  }

  RemotePeer.remoteUnchoked = function (peers) {
    return _.select(peers || RemotePeer.all(), function(peer) { return peer.remoteChoked === false });
  }

  RemotePeer.localInterested = function (peers) {
    return _.select(peers || RemotePeer.all(), function(peer) { return peer.localInterested === true });
  }

  RemotePeer.remoteInterested = function (peers) {
    return _.select(peers || RemotePeer.all(), function(peer) { return peer.remoteInterested === true });
  }

  RemotePeer.bothInterested = function (peers) {
    return RemotePeer.remoteInterested(RemotePeer.localInterested(peers));
  }

  RemotePeer.notRequesting = function (peers) {
    return _.select(peers || RemotePeer.all(), function(peer) { return peer.requesting === false });
  }

  RemotePeer.findRarestChunk = function (peers) {
    return RarestChunkHelper.findRarestChunk(MediaSourceProxy.getBitfield(), peers);
  }

  RemotePeer.findRandomUnexistingChunk = function(peers) {
    return RarestChunkHelper.findRandomUnexistingChunk(MediaSourceProxy.getBitfield(), peers);
  }

  RemotePeer.random = function (peers) {
    peers = peers || RemotePeer.all();
    return peers[_.random(peers.length - 1)] || [];
  }

  RemotePeer.active = function (peers) {
    return _.select(peers || RemotePeer.all(), function(peer) { return peer.isActive() });
  }

  RemotePeer.inactive = function (peers) {
    return _.select(peers || RemotePeer.all(), function(peer) { return !peer.isActive() });
  }

  RemotePeer.sortByByteRate = function (peers) {
    return _.sortBy(peers, function(peer) { return peer.averageByteRate() * -1 });
  }

  RemotePeer.bestTalkingPeers = function () {
    var bothInterested = RemotePeer.bothInterested();
    var candidates = RemotePeer.sortByByteRate(bothInterested);

    if (candidates.length < UNCHOKED_LIMIT) {
      var localInterested = RemotePeer.localInterested();
      var sortedLocalInterested = RemotePeer.sortByByteRate(localInterested);

      candidates = _.uniq(candidates.concat(sortedLocalInterested));
    }

    return _.first(candidates, UNCHOKED_LIMIT);
  }

  return RemotePeer;

});
