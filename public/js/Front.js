define('Front', function () {

  'use strict';

  var COLORS = ['turquoise', 'wet-asphalt', 'green-sea', 'nephritis', 'belize-hole', 'wisteria',
                'midnight-blue', 'sun-flower', 'carrot', 'clouds', 'concrete', 'pumpkin', 'pomegranate', 'silver',
                'blue6', 'blue5', 'blue4', 'blue2', 'blue3', 'blue1', 'grey'];

  var ACTIVITY_WIDTH = 100;

  var topics = [];
  var topicTables = {};
  var activityTables = {};
  var activityCount = 0;
  var freeColors = COLORS.slice(0, COLORS.length);

  var chunkBar;
  var topicsContainer;
  var activitiesContainer;
  var body;

  var initialize = function () {
    if (!FRONT_ENABLED) { return; }

    chunkBar            = document.querySelector('#chunks-bar');
    topicsContainer     = document.querySelector('.topics-inner');
    activitiesContainer = document.querySelector('.activities-inner');
    body                = document.body;
  }

  var findTopic = function (topicName) {
    return _.findWhere(topics, {name: topicName});
  }

  var findOrCreateTopic = function(topicName) {
    var topic = findTopic(topicName);

    if (_.isUndefined(topic)) {
      topic = {name: topicName, requestedAmount: 0, color: getFreeColor()}
      topics.push(topic);
    }

    return topic;
  }

  var getRandomColor = function() {
    return COLORS[Math.floor(Math.random() * COLORS.length)];
  }

  var getFreeColor = function() {
    return _.isEmpty(freeColors) ? getRandomColor() : freeColors.pop();
  }

  //
  //
  // Title

  var renderTitle = function (titleId) {
    var titleIdSpan = document.querySelector('#title-id');
    titleIdSpan.innerHTML = titleId;
  }

  //
  //
  // Paint Chunks

  var renderChunkBar = function (amountOfChunks) {
    if (!FRONT_ENABLED) { return; }

    var chunksDivs = '';
    _.each(_.range(amountOfChunks), function() { chunksDivs += '<div class="chunk"></div>' });
    chunkBar.innerHTML = chunksDivs;
  }

  var paintChunkAsLoaded = function(index, topicName) {
    if (!FRONT_ENABLED) { return; }

    var topic = findTopic(topicName);
    chunkBar.childNodes[index].className = 'chunk ' + topic.color;
  }

  var paintChunkAsBuffered = function(index) {
    if (!FRONT_ENABLED) { return; }

    addClassToNode(chunkBar.childNodes[index], 'buffered');
  }

  var animateChunkAsDiscarded = function(index) {
    if (!FRONT_ENABLED) { return; }

    var chunk = chunkBar.childNodes[index];
    addClassToNode(chunk, 'discarded');

    setInterval(function () {
      removeClassFromNode(chunk, 'discarded');
    }, 300);
  }

  //
  //
  // Peers

  var paintPeerAsUnchoked = function (topicName, scenario) {
    if (!FRONT_ENABLED) { return; }

    var tableId = topicName.replace(' ', '') + '-topic-table';
    var scenarioDiv = topicsContainer.querySelector('#' + tableId + ' div.' + scenario);

    addClassToNode(scenarioDiv, scenario + '-unchoked')
  }

  var paintPeerAsChoked = function (topicName, scenario) {
    if (!FRONT_ENABLED) { return; }

    var tableId = topicName.replace(' ', '') + '-topic-table';
    var scenarioDiv = topicsContainer.querySelector('#' + tableId + ' div.' + scenario);

    removeClassFromNode(scenarioDiv, scenario + '-unchoked');
  }

  var paintPeerAsInactive = function (topicName) {
    if (!FRONT_ENABLED) { return; }

    paintPeerAsChoked(topicName, 'local');
    paintPeerAsChoked(topicName, 'remote');

    addClassToNode(topicTables[topicName], 'inactive');
    addClassToNode(activityTables[topicName], 'inactive');
  }

  //
  //
  // Increment

  var incrementTopic = function (topicName) {
    if (!FRONT_ENABLED) { return; }

    var topic = findTopic(topicName);

    var index = _.indexOf(topics, topic);
    topic = {name: topic.name, requestedAmount: (topic.requestedAmount + 1), color: topic.color};
    topics[index] = topic;

    var tableId = topic.name.replace(' ', '') + '-topic-table';
    var topicNumber = topicsContainer.querySelector('#' + tableId + ' span.number');
    topicNumber.innerHTML = topic.requestedAmount;
  }

  var updateStatistic = function (topicName, statistic) {
    if (!FRONT_ENABLED) { return; }

    var tableId = topicName.replace(' ', '') + '-topic-table';
    var topicStatistic = topicsContainer.querySelector('#' + tableId + ' span.statistic');
    topicStatistic.innerHTML = statistic;
  }

  //
  //
  // Activities

  var renderActivity = function (activity) {
    if (!FRONT_ENABLED) { return; }

    var topic = findOrCreateTopic(activity.topic);

    prepareActivityTable(topic);
    prepareTopicTable(topic);
    activityCount += 1;

    _.each(activityTables, function (activityTable, topicName) {
      appendActivityToTable(activityTable, topicName, activity)
    });

    activitiesContainer.scrollLeft = activityCount * ACTIVITY_WIDTH;
  }

  var prepareTopicTable = function (topic) {
    var topicTable = topicTables[topic.name];

    if (_.isUndefined(topicTable)) {
      var tableId = topic.name.replace(' ', '') + '-topic-table';
      appendTopicTable(tableId, topic);

      topicTable = topicsContainer.querySelector('#' + tableId);
      topicTables[topic.name] = topicTable;
    } else {
      removeClassFromNode(topicTable, 'inactive');
    }
  }

  var appendTopicTable = function (id, topic) {
    var topicTable = document.createElement('li');

    var localDiv = document.createElement('div');
    localDiv.className = 'local';
    var innerTopicTable = document.createElement('div');
    innerTopicTable.className = 'inner ' + topic.color;
    var remoteDiv = document.createElement('div');
    remoteDiv.className = 'remote';

    topicTable.id = id;
    topicTable.className = 'topic-table';

    var nameSpan = document.createElement('span');
    nameSpan.className = 'name';
    nameSpan.innerHTML = topic.name;
    var numberSpan = document.createElement('span');
    numberSpan.className = 'number';
    numberSpan.innerHTML = '&nbsp;';
    var statisticSpan = document.createElement('span');
    statisticSpan.className = 'statistic';
    statisticSpan.innerHTML = '&nbsp;';

    innerTopicTable.appendChild(nameSpan);
    innerTopicTable.appendChild(numberSpan);
    innerTopicTable.appendChild(statisticSpan);
    topicTable.appendChild(localDiv);
    topicTable.appendChild(innerTopicTable);
    topicTable.appendChild(remoteDiv);
    topicsContainer.appendChild(topicTable);
  }

  var prepareActivityTable = function (topic) {
    var activityTable = activityTables[topic.name];

    if (_.isUndefined(activityTables[topic.name])) {
      var tableId = topic.name.replace(' ', '') + '-activity-table';
      appendActivityTable(tableId);

      activityTable = activitiesContainer.querySelector('#' + tableId);
      activityTables[topic.name] = activityTable;

      var emptyActivities = '';
      _(activityCount).times(function () { emptyActivities += emptyActivity() });
      activityTable.innerHTML = emptyActivities;
    } else {
      removeClassFromNode(activityTable, 'inactive');
    }
  }

  var appendActivityTable = function (id) {
    var activityTable = document.createElement('li'),
        activityRow = document.createElement('ul');

    activityTable.id = id;
    activityTable.className = 'activity-table';
    activityRow.className = 'activity-row';
    activitiesContainer.appendChild(activityTable);
  }

  var appendActivityToTable = function (activityTable, topicName, activity) {
    if (topicName === activity.topic) {
      var activity = createActivity(activity.data);
      activityTable.appendChild(activity);
    } else {
      activityTable.innerHTML += emptyActivity();
    }
  }

  var createActivity = function (data) {
    var activity = document.createElement('li'),
        upNode = document.createElement('p'),
        downNode = document.createElement('p');

    activity.className = 'activity';
    upNode.className = 'up';
    upNode.innerHTML = data.up;
    downNode.className = 'down';
    downNode.innerHTML = data.down;
    activity.appendChild(upNode);
    activity.appendChild(downNode);

    return activity;
  }

  var emptyActivity = function () {
    return '<li class="activity"></li>';
  }

  //
  //
  // Request Window

  var paintRequestWindow = function (requestWindow) {
    if (!FRONT_ENABLED) { return; }

    body.className = requestWindow;
  }

  //
  //
  // Utils

  var addClassToNode = function (node, className) {
    node.className += ' ' + className;
  }

  var removeClassFromNode = function (node, className) {
    var classToBeRemoved = ' ' + className;
    var re = new RegExp(classToBeRemoved, 'g');

    node.className = node.className.replace(re, '');
  }

  return {
    initialize: initialize,
    renderTitle: renderTitle,
    renderChunkBar: renderChunkBar,
    paintChunkAsLoaded: paintChunkAsLoaded,
    paintChunkAsBuffered: paintChunkAsBuffered,
    animateChunkAsDiscarded: animateChunkAsDiscarded,
    paintPeerAsUnchoked: paintPeerAsUnchoked,
    paintPeerAsChoked: paintPeerAsChoked,
    paintPeerAsInactive: paintPeerAsInactive,
    incrementTopic: incrementTopic,
    updateStatistic: updateStatistic,
    renderActivity: renderActivity,
    paintRequestWindow: paintRequestWindow
  }

});
