define('ChunkSerializer', function () {

  'use strict';

  var ChunkSerializer = function (chunkId, peerId, totalPieces) {
    this.instances.push(this);
    this.chunkId = chunkId;
    this.totalPieces = totalPieces;
    this.peerId = peerId;
    this.chunk = new Array(totalPieces);

    this.appendPiece = function (index, piece) {
      this.chunk[index] = piece;
    }

    this.chunkIsFull = function () {
      return _.compact(this.chunk).length === totalPieces;
    }

    this.getChunk = function () {
      return _.flatten(this.chunk);
    }
  }

  ChunkSerializer.prototype.instances = ChunkSerializer.prototype.instances || [];

  ChunkSerializer.prototype.instance = function (chunkId, peerId, totalPieces) {
    var serializer = ChunkSerializer.prototype.findInstanceForChunk(chunkId, peerId);

    if (serializer) {
      return serializer;
    } else {
      return new ChunkSerializer(chunkId, peerId, totalPieces);
    }
  }

  ChunkSerializer.prototype.findInstanceForChunk = function (chunkId, peerId) {
    return _.findWhere(ChunkSerializer.prototype.instances, { chunkId: chunkId, peerId: peerId });
  }

  ChunkSerializer.prototype.forgetInstance = function (instance) {
    ChunkSerializer.prototype.instances = _.reject(ChunkSerializer.prototype.instances, function (instanceInList) {
      return _.isEqual(instance, instanceInList);
    });
  }

  return ChunkSerializer;

});
