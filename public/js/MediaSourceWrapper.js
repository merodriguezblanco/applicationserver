define('MediaSourceWrapper', ['VideoWrapper', 'Front'],
       function (VideoWrapper, Front) {

  'use strict';

  var END_OF_STREAM = 'Stream has ended!';
  var QUEUE_CLEANER_INTERVAL = 100; // milliseconds

  var buffer;
  var bufferQueue;
  var media;
  var timer;

  var initialize = function (metaData) {
    media = new MediaSource();
    media.addEventListener('sourceopen', onSourceOpen.bind(this, metaData));
    bufferQueue = [];
    timer = null;

    VideoWrapper.initialize();
    VideoWrapper.setMediaSource(media);
  }

  var onSourceOpen = function (metaData) {
    buffer = media.addSourceBuffer(metaData.type);
    buffer.addEventListener('update', onBufferAppendEnded);
    buffer.addEventListener('error', onBufferAppendError);
    buffer.addEventListener('abort', onBufferAppendAborted);

    event = new CustomEvent('mediaSourceOpen');
    globalEventListener.dispatchEvent(event);
  }

  var onBufferAppendEnded = function () {
    if (!isBuffering() && bufferQueue.length > 0) {
      appendNextChunkToBuffer();
    }

    if (!timer && bufferQueue.length > 0) {
      timer = setTimeout(onBufferAppendEnded, QUEUE_CLEANER_INTERVAL);
    }
  }

  var onBufferAppendError = function (error) {
    console.log('WARNING: onBufferAppendError', error);
  }

  var onBufferAppendAborted = function () {
    console.log('WARNING: onBufferAppendAborted');
  }

  var queueInBuffer = function (data) {
    bufferQueue.push(data);
    if (!isBuffering()) {
      appendNextChunkToBuffer();
    }
  }

  var appendNextChunkToBuffer = function () {
    var chunk = bufferQueue.shift();
    if (bufferQueue.length === 0) { timer = null; }

    if (chunk) {
      if (chunk === END_OF_STREAM) {
        if (media.readyState === 'open') {
          media.endOfStream();
          logActivity('end of stream', 'media source');
        }
      } else {
        buffer.appendBuffer(new Uint8Array(chunk));
      }
    }
  }

  var isBuffering = function () {
    return buffer.updating;
  }

  var getSourceBuffer = function () {
    return buffer;
  }

  var closeStream = function () {
    queueInBuffer(END_OF_STREAM);
  }

  var logActivity = function (upText, downText) {
    Front.renderActivity({ topic: 'General', data: { up: upText, down: downText } });
  }

  return {
    initialize: initialize,
    getSourceBuffer: getSourceBuffer,
    queueInBuffer: queueInBuffer,
    closeStream: closeStream
  }

})
