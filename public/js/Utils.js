define('Utils', function () {

  'use strict';

  var objectToURLParams = function (object, prefix) {
    var params = [];

    for (var key in object) {
      if (object.hasOwnProperty(key)) {
        var subPrefix = prefix ? prefix + '[' + key + ']' : key,
            subObject = object[key];

        if (typeof subObject == 'object') {
          params.push(objectToURLParams(subObject, subPrefix));
        } else {
          params.push(encodeURIComponent(subPrefix) + '=' + encodeURIComponent(subObject));
        }
      }
    }

    return params.join('&');
  }

  _.mixin({
    objectToURLParams: objectToURLParams
  });

});
