define('Broker', ['MediaSourceProxy', 'CDN', 'HostPeer', 'Front'],
  function (MediaSourceProxy, CDN, HostPeer, Front) {

  'use strict';

  var TIMEOUT = 1000;
  var RANDOM_DISTRIBUTION = 3;
  var videoFileName;
  var requestCounter = 0;

  var initialize = function(videoName, trackerInfo) {
    videoFileName = videoName;

    _.each(trackerInfo['cdns'], function(cdn) {
      CDN.create(cdn);
    });

    MediaSourceProxy.initialize();
    HostPeer.initialize(trackerInfo['host']);
  }

  var start = function() {
    logActivity('start');
    request();
  }

  var fireFileCompleted = function () {
    var event = new CustomEvent('fileCompleted');
    globalEventListener.dispatchEvent(event);
  }

  var request = function () {
    if (MediaSourceProxy.isFileCompleted()) {
      logActivity('file completed');
      fireFileCompleted();
      return;
    }

    if (MediaSourceProxy.isUrgentWindow()) {
      requestUrgent();
    } else if (MediaSourceProxy.isNormalWindow()) {
      HostPeer.requestRarestChunk();
      requestNormal();
    } else if (MediaSourceProxy.isFutureWindow()) {
      requestFuture();
    }
  }

  var requestUrgent = function() {
    logRequestWindow('urgent');
    var cdn = getUrgentCDN();

    requestUrgentChunkToCDN(cdn);
    scheduleNextRequest();
  }

  var getUrgentCDN = function() {
    var sortedCDNs = CDN.byAverageRtt();

    return sortedCDNs[0];
  }

  var requestNormal = function() {
    logRequestWindow('normal');
    var cdn = getNormalCDN();

    requestNormalChunkToCDN(cdn);
    scheduleNextRequest();
  }

  var getNormalCDN = function() {
    return CDN.getByRandomWeight();
  }

  var requestFuture = function() {
    logRequestWindow('future');
    HostPeer.requestRarestChunk();
    scheduleNextRequest();
  }

  var requestCDNData = function(chunkNumber) {
    return {
      videoFileName: videoFileName,
      chunk: MediaSourceProxy.getCDNRequestData(chunkNumber)
    }
  }

  var requestUrgentChunkToCDN = function(cdn) {
    var chunkNumber = MediaSourceProxy.getNextChunkNumber();

    if (!_.isUndefined(chunkNumber)) {
      MediaSourceProxy.markChunkAsRequested(chunkNumber);
      cdn.requestChunk(requestCDNData(chunkNumber));
    }
  }

  var requestNormalChunkToCDN = function(cdn) {
    requestCounter = (requestCounter + 1) % RANDOM_DISTRIBUTION;
    var chunkNumber;

    if (requestCounter === 0) {
      chunkNumber = HostPeer.findRandomUnexistingChunk();
    }

    if (_.isUndefined(chunkNumber) || (requestCounter > 0)) {
      chunkNumber = MediaSourceProxy.getNextChunkNumber();
    }

    if (!_.isUndefined(chunkNumber)) {
      MediaSourceProxy.markChunkAsRequested(chunkNumber);
      cdn.requestChunk(requestCDNData(chunkNumber));
    }
  }

  var scheduleNextRequest = function () {
    setTimeout(function() {
      request();
    }, TIMEOUT);
  }

  var logActivity = function(activity) {
    setTimeout(Front.renderActivity({ topic: 'General', data: { up: activity, down: 'broker' } }), 0);

  }

  var logRequestWindow = function(requestWindow) {
    setTimeout(Front.paintRequestWindow(requestWindow), 0);
  }

  return {
    initialize: initialize,
    start: start
  }

});
