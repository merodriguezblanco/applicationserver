define('ChunkStreamer', function () {

  'use strict';

  var ChunkStreamer = function (chunk) {
    this.pieces = chunk;
    this.totalPieces = Math.ceil(this.pieces.length / this.MTU);
    this.currentPieceNumber = -1;
    this.currentPiece = null;
    this.streaming = true;

    this.setCurrentPiece = function (currentPiece) {
      this.currentPieceNumber++;
      this.currentPiece = currentPiece;
    }

    this.nextPiece = function () {
      var head, tail;

      if (this.pieces.length >= this.MTU) {
        head = this.pieces.slice(0, this.MTU);
        tail = this.pieces.slice(this.MTU, this.pieces.length);
      } else {
        head = this.pieces.slice(0, this.pieces.length);
        tail = [];
        this.streaming = false;
      }

      this.pieces = tail;
      this.setCurrentPiece(head);
    }

    this.stream = function (callback) {
      while(this.streaming) {
        this.nextPiece();
        callback();
      }
    }
  }

  ChunkStreamer.prototype.MTU = 10000; // Encontrar el numero justo aca!

  return ChunkStreamer;

});

