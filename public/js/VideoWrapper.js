define('VideoWrapper', function () {

  'use strict';

  // Singleton class.
  var VideoWrapper = (function () {

    var instance;

    var init = function () {
      var self = this,
          video,
          isWaitingForPlay        = false,
          isWaitingForStartupPlay = true,
          totalWaitingTime        = 0,
          startupWaitingTime      = 0,
          waitingTimeAmount       = 0,
          startWaitingTime;

      var videoWaiting = function () {
console.log('videoWaiting');
        if (!isWaitingForPlay) {
          startWaitingTime = new Date();
          waitingTimeAmount += 1;
          isWaitingForPlay = true;
        }
      }

      var videoCanPlay = function () {
console.log('videoCanPlay');
        var waitedTime;

        waitedTime = new Date() - startWaitingTime;
        totalWaitingTime += waitedTime;
        isWaitingForPlay = false;

        if (isWaitingForStartupPlay) {
          startupWaitingTime += waitedTime;
          isWaitingForStartupPlay = false;
        }
      }

      var videoEnded = function () {
console.log('videoEnded');
        isWaitingForPlay = false;
      }

      var initialize = function () {
        video = document.querySelector('video');

        video.addEventListener('waiting', videoWaiting);
        video.addEventListener('loadstart', videoWaiting);
        video.addEventListener('canplay', videoCanPlay);
        video.addEventListener('ended', videoEnded);
      }

      var setMediaSource = function (mediaSource) {
        video.src = URL.createObjectURL(mediaSource);
      }

      var getExecutionTime = function () {
        if (!_.isUndefined(video)) {
          return video.currentTime;
        } else {
          return 0;
        }
      }

      var isEnded = function () {
        return video.ended;
      }

      var getWaitingForPlayTime = function () {
        return totalWaitingTime;
      }

      var getWaitingForStartupPlayTime = function () {
        return startupWaitingTime;
      }

      var videoStats = function () {
        var now, waitedTime;

        // In case video is stopped, we should send current waiting time.
        if ((isWaitingForPlay || isWaitingForStartupPlay) && !_.isUndefined(startWaitingTime)) {
          now = new Date();

          waitedTime = now - startWaitingTime;
          totalWaitingTime += waitedTime;

          if (isWaitingForStartupPlay) {
            startupWaitingTime += waitedTime;
          }

          startWaitingTime = now;
        }

console.log('isWaitingForStartupPlay', isWaitingForStartupPlay);
console.log('isWaitingForPlay', isWaitingForPlay);
console.log('startWaitingTime', startWaitingTime);
console.log('totalWaitingTime', totalWaitingTime);
console.log('startupWaitingTime', startupWaitingTime);
console.log('waitingTimeAmount', waitingTimeAmount);
console.log('****************\n');

        return {
          totalWaitingTime: totalWaitingTime,
          startupWaitingTime: startupWaitingTime,
          waitingTimeAmount: waitingTimeAmount
        }
      }

      return {
        initialize: initialize,
        setMediaSource: setMediaSource,
        getExecutionTime: getExecutionTime,
        isEnded: isEnded,
        getVideoStats: videoStats
      }
    }

    return {
      getInstance: function () {
        if (!instance) {
          instance = init();
        }

        return instance;
      }
    }

  }());

  return VideoWrapper.getInstance();

})
