define('HostPeer', ['RemotePeer', 'MediaSourceProxy', 'CDN', 'Front', 'VideoWrapper', 'require', 'Tracker'],
       function (RemotePeer, MediaSourceProxy, CDN, Front, VideoWrapper, require, Tracker) {

  'use strict';

  var TRACKER_KEEP_ALIVE_INTERVAL    = 3000, //milliseconds
      TIT_FOR_TAT_INTERVAL           = 10000, //milliseconds
      OPTIMISTIC_UNCHOKING_INTERVAL  = 30000, //milliseconds
      MIN_SWARM                      = 10,
      TIT_FOR_TATS_PER_OPT_UNCHOKING = 3;

  var HostPeer         = HostPeer || {},
      id               = null,
      fileCompleted    = false,
      peerJs           = null,
      titForTatCounter = 1;

  var titForTatInterval;

  HostPeer.initialize = function (hostPeer) {
    id = hostPeer.id
    initializePeerJs(id);

    installTimers();
    globalEventListener.addEventListener('fileCompleted', onFileCompleted);

    setTimeout(Front.renderTitle(id), 0);
  }

  HostPeer.getId = function () {
    return id;
  }

  HostPeer.getFileCompleted = function () {
    return fileCompleted;
  }

  HostPeer.getExecutionChunkNumber = function () {
    return MediaSourceProxy.getExecutionChunkNumber();
  }

  var onFileCompleted = function () {
    fileCompleted = true;
    Tracker().notifyFileCompleted();
    swapIntervals();
  }

  var swapIntervals = function () {
    clearInterval(titForTatInterval);
    setInterval(function() {
      keepAliveAndPurge();
    }, TRACKER_KEEP_ALIVE_INTERVAL);
  }

  var initializePeerJs = function (hostPeerId) {
    var peerJs = new Peer(hostPeerId, {
      host: 'peerjs2-streamio.rhcloud.com',
      port: 8000,
      debug: false,
      config: {
        iceServers: [
          { url: 'stun:stun.l.google.com:19302' },
          { url: 'turn:numb.viagenie.ca:3478',
            username: 'marcelocasiraghi@hotmail.com',
            credential: 'testing' } ] }
    });

    RemotePeer.prototype.peerJs = peerJs;
    peerJs.on('connection', onConnection);
  }

  var installTimers = function () {
    setInterval(function () {
      Tracker().sendKeepAlive(buildResourceStats(), buildVideoStats());
    }, TRACKER_KEEP_ALIVE_INTERVAL);

    titForTatInterval = setInterval(titForTat, TIT_FOR_TAT_INTERVAL);

    HostPeer.setupSwarm();
  }

  HostPeer.setupSwarm = function () {
    globalEventListener.addEventListener('swarmReceived', proccessSwarm);

    Tracker().requestSwarm();
  }

  var proccessSwarm = function(event) {
    var peers = event.data['peers'];

    var newPeers = RemotePeer.create(peers);
    RemotePeer.tryConnect(newPeers);
  }

  var buildResourceStats = function () {
    return _.extend(CDN.buildStats(), RemotePeer.buildStats());
  }

  var buildVideoStats = function () {
    var videoStats = VideoWrapper.getVideoStats();
    return videoStats;
  }

  var logActivity = function (upText, downText) {
    setTimeout(Front.renderActivity({ topic: 'General', data: { up: upText, down: downText } }), 0);
  }

  //
  //
  // P2P

  var onConnection = function (connection) {
    var bitfield = connection.metadata.bitfield;

    if (MediaSourceProxy.isFileCompleted() || MediaSourceProxy.hasNeededChunks(bitfield)) {
      var remotePeer = RemotePeer.findOrCreate(connection.peer);
      var executionChunkNumber = connection.metadata.executionChunkNumber;
      remotePeer.onConnectionResponse(bitfield, executionChunkNumber);

      connection.on('data',  remotePeer.onDataReceived);
      connection.on('error', remotePeer.onError);
    }
  }

  HostPeer.evaluateUninterestForPeers = function (chunkNumber) {
    RemotePeer.evaluateUninterest(chunkNumber);
  }

  HostPeer.sendHaveToPeers = function (chunkNumber) {
    RemotePeer.sendHave(chunkNumber);
  }

  HostPeer.getResource = function (resourceId, resourceType) {
    if (resourceType === 'cdn') {
      return CDN.find(resourceId);
    } else if (resourceType === 'peer') {
      return RemotePeer.find(resourceId);
    } else {
      console.log('ERROR:', 'Unsupported resourceType', resourceType);
    }
  }

  var keepAliveAndPurge = function () {
    var executionChunkNumber = MediaSourceProxy.getExecutionChunkNumber();
    RemotePeer.sendKeepAlive(executionChunkNumber); // Send KeepAlive and stats BEFORE PURGING
    RemotePeer.purge();
  }

  var titForTat = function () {
    logActivity('tit for tat', 'host peer');

    keepAliveAndPurge();
    requestMorePeersIfNeeded();

    var newTalkingPeers = RemotePeer.bestTalkingPeers();
    var oldTalkingPeers = _.difference(RemotePeer.localUnchoked(), newTalkingPeers);
    RemotePeer.tryUnchoke(newTalkingPeers);
    RemotePeer.tryChoke(oldTalkingPeers);

    RemotePeer.shiftByteRates();

    if (titForTatCounter === 0) {
      optimisticUnchocking();
    }

    titForTatCounter = (titForTatCounter + 1) % TIT_FOR_TATS_PER_OPT_UNCHOKING;
  }

  var optimisticUnchocking = function () {
    logActivity('opt. unchoking', 'host peer');

    var optimisticRemotePeer = getOptimisticRemotePeer();

    if (optimisticRemotePeer !== undefined) {
      optimisticRemotePeer.interested();
      optimisticRemotePeer.unchoke();
    }
  }

  var getOptimisticRemotePeer = function () {
    // TODO: enhance so that it's also the best option based on its window
    var handshakeEstablishedPeers = RemotePeer.handshakeEstablished();
    var chockedPeers              = RemotePeer.localChoked(handshakeEstablishedPeers);
    var interestedPeers           = RemotePeer.remoteInterested(chockedPeers);

    return interestedPeers[0] || chockedPeers[0] || RemotePeer.random(handshakeEstablishedPeers)[0];
  }

  var requestMorePeersIfNeeded = function () {
    if (_.size(RemotePeer.all()) < MIN_SWARM) {
      Tracker().requestSwarm();
    }
  }

  HostPeer.requestRarestChunk = function () {
    var unchokedPeers  = RemotePeer.remoteUnchoked();
    var availablePeers = RemotePeer.notRequesting(unchokedPeers);

    if (!_.isEmpty(availablePeers)) {
      var rarestChunkData = RemotePeer.findRarestChunk(availablePeers);

      // Momentary there could be peers but nobody has chunks that I need
      if (rarestChunkData) {
        var rarestPeer  = rarestChunkData.peer;
        var rarestChunk = rarestChunkData.chunkNumber;

        if (rarestPeer) { rarestPeer.requestChunk(rarestChunk); }
      }
    }
  }

  HostPeer.findRandomUnexistingChunk = function () {
    var chunk = RemotePeer.findRandomUnexistingChunk();
    return chunk ? chunk.chunkNumber : undefined;
  }

  // Needed to resolve circular dependency
  var Tracker = function () {
    return require('Tracker');
  }

  return HostPeer;

});
