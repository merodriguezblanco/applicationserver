define('CDN', ['Front'], function (Front) {

  'use strict';

  var XHR_AJAX_COMPLETE = 4,
      XHR_SUCCESS_PARTIAL_CONTENT = 206,
      INITIAL_RTT = 0,
      LAST_RTT_COEF = 0.5,
      SECOND_LAST_RTT_COEF = 0.3,
      THIRD_LAST_RTT_COEF = 0.2;


  var CDN = function (cdnData) {

    var url = cdnData.url;
    var id = cdnData.name;
    var weight = cdnData.weight;
    var rtts = [INITIAL_RTT, INITIAL_RTT, INITIAL_RTT];
    var averageRtt = INITIAL_RTT;
    var chunksDelivered = 0;
    var chunksDeliveredReported = 0;

    var self = this;

    self.getId = function() {
      return id;
    }

    self.getWeight = function() {
      return weight;
    }

    self.getAverageRtt = function() {
      return averageRtt;
    }

    self.getChunksDelivered = function () {
      return chunksDelivered;
    }

    self.buildStats = function () {
      return {
        type: 'cdn',
        numbers: {
          chunksDelivered: getChunksDeliveredSinceLastReport()
        }
      };
    }

    var getChunksDeliveredSinceLastReport = function () {
      var difference = chunksDelivered - chunksDeliveredReported;
      chunksDeliveredReported = chunksDelivered;
      return difference;
    }

    var chunkDelivered = function () {
      chunksDelivered++;
    }

    self.requestChunk = function (data) {
      logActivity(data.chunk.number, 'request to');

      var xhr = new XMLHttpRequest();
      var beginning = data.chunk.offset;
      var end = data.chunk.offset + data.chunk.size - 1;

      xhr.open('GET', url + '/' + data.videoFileName, true);
      xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
      xhr.setRequestHeader('Range', 'bytes='+ beginning +'-'+ end);
      xhr.responseType = 'arraybuffer';
      xhr.onreadystatechange = function (event) {
        if (xhr.readyState === XHR_AJAX_COMPLETE && xhr.status === XHR_SUCCESS_PARTIAL_CONTENT) {
          var chunkData = {};
          chunkData['chunkNumber'] = data.chunk.number;
          chunkData['binary'] = _.values(JSON.parse(JSON.stringify(new Uint8Array(xhr.response))));
          receiveChunk(chunkData);
        }
      }
      xhr.send();
      notifyOnChunkRequested(data.chunk);
    }

    var receiveChunk = function (data) {
      if (_.isNull(data)) {
        logActivity(data.chunkNumber, 'receive empty from');
        notifyOnChunkEmpty(data);
      } else {
        logActivity(data.chunkNumber, 'receive from');
        notifyOnChunkReceived(data);
      }
    }

    self.failChunk = function (chunkNumber) {
      logActivity(chunkNumber, 'failed');
      notifyOnChunkFailed({ chunkNumber: chunkNumber });
    }

    var notifyOnChunkReceived = function (data) {
      notifyEvent(data, 'documentOnChunkReceived');
    }

    var notifyOnChunkEmpty = function (data) {
      notifyEvent(data, 'documentOnChunkEmpty');
    }

    var notifyOnChunkFailed = function (data) {
      notifyEvent(data, 'documentOnChunkFailed');
    }

    var notifyOnChunkRequested = function (data) {
      notifyEvent(data, 'documentOnChunkRequested');
    }

    var notifyEvent = function (data, eventName) {
      var event = new CustomEvent(eventName);
      data['resourceId'] = id;
      data['resourceType'] = 'cdn';
      event.data = data;
      globalEventListener.dispatchEvent(event);
    }

    self.updateAverageRtt = function(sampleRtt) {
      rtts = _.tail(rtts).concat([sampleRtt]);
      averageRtt = rtts[0] * THIRD_LAST_RTT_COEF +
                   rtts[1] * SECOND_LAST_RTT_COEF +
                   rtts[2] * LAST_RTT_COEF;
      setTimeout(Front.updateStatistic(id, parseFloat(averageRtt / 1000).toFixed(2) + 's'), 0);
    }

    // Triggered when the chunk is loaded in the MediaSourceProxy
    self.updateStatsOnChunkLoaded = function (chunk) {
      chunkDelivered();
    }

    var logActivity = function (upText, downText) {
      setTimeout(Front.renderActivity({ topic: id, data: { up: upText, down: downText } }), 0);
    }

  }

  CDN.cdns = []

  CDN.create = function (cdnData) {
    var cdn = new CDN(cdnData);
    CDN.cdns.push(cdn);
  }

  CDN.find = function (cdnId) {
    return _.find(CDN.all(), function (cdn) {
      return cdn.getId() === cdnId;
    });
  }

  CDN.all = function () {
    return CDN.cdns;
  }

  CDN.byAverageRtt = function () {
    return _.sortBy(CDN.all(), function(cdn) { return cdn.getAverageRtt() });
  }

  CDN.getByRandomWeight = function () {
    var weightedCDNs = [];
    _.each(CDN.all(), function(cdn) {
      _(cdn.getWeight()).times(function(n){
        weightedCDNs.push(cdn);
      });
    });

    return weightedCDNs[_.random(weightedCDNs.length - 1)];
  }

  CDN.buildStats = function () {
    var stats = {};
    _.each(CDN.all(), function (cdn) {
      stats[cdn.getId()] = cdn.buildStats();
    });
    return stats;
  }

  return CDN;

});
