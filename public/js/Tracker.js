define('Tracker', ['TorrentParser', 'Front', 'HostPeer'], function (TorrentParser, Front, HostPeer) {

  'use strict';

  var Tracker = {} || Tracker,
    url    = TorrentParser.parseTrackers()[0].url,
    socket = null;

  var isConnected = function () {
    return (!_.isNull(socket));
  }

  var connect = function () {
    socket = io.connect(url);

    socket.on('peerInitialized', receivePeerInfo);
    socket.on('swarmReceived', receiveSwarm);
  }

  var receivePeerInfo = function (data) {
    logActivity('initialize', 'tracker');

    var cdns = data['cdns'];
    var hostPeer = data['host'];

    event = new CustomEvent('peerInitialized');
    event.data = { 'host': hostPeer, 'cdns': cdns };
    globalEventListener.dispatchEvent(event);
  }

  var receiveSwarm = function (data) {
    logActivity('swarm', 'tracker');

    var remotePeers = data['peers'];

    event = new CustomEvent('swarmReceived');
    event.data = { 'peers': remotePeers }
    globalEventListener.dispatchEvent(event);
  }

  var sendMessage = function (emitKey, emitData) {
    if (!isConnected()) {
      connect();
    }
    socket.emit(emitKey, emitData);
  }

  Tracker.initHostPeer = function () {
    sendMessage('initHostPeer');
  }

  Tracker.requestSwarm = function () {
    sendMessage('requestSwarm', { peerId: HostPeer.getId(), index: HostPeer.getExecutionChunkNumber() })
  }

  Tracker.sendKeepAlive = function (remoteStats, videoStats) {
    sendMessage('keepAlive', {
      peerId: HostPeer.getId(),
      index: HostPeer.getExecutionChunkNumber(),
      fileCompleted: HostPeer.getFileCompleted(),
      remoteStats: remoteStats,
      videoStats: videoStats
    });
  }

  var logActivity = function (upText, downText) {
    setTimeout(Front.renderActivity({ topic: 'General', data: { up: upText, down: downText } }), 0);
  }

  Tracker.notifyFileCompleted = function () {
    sendMessage('fileCompleted', { peerId: HostPeer.getId() })
  }

  return Tracker;
});
