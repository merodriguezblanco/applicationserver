define('ResponseTimer', function () {

  'use strict';

  var MAX_WAIT = 20000; // All hope is lost after this amount of time!

  var ResponseTimer = function(chunkNumber, resource) {

    var self = this;

    this.chunkNumber = chunkNumber;
    this.resource = resource;
    this.startTime = new Date().getTime();
    this.timeout = setTimeout(function() {
      updateResourceRtt();
      self.resource.failChunk(self.chunkNumber);
    }, MAX_WAIT);

    var updateResourceRtt = function () {
      var stopTime = new Date().getTime();
      var sampleRtt = stopTime - self.startTime;
      self.resource.updateAverageRtt(sampleRtt);
    }

    self.getChunkNumber = function() {
      return self.chunkNumber;
    }

    self.getResource = function() {
      return self.resource;
    }

    self.stopTimer = function() {
      updateResourceRtt();
      clearTimeout(self.timeout);
    }

  }

  ResponseTimer.find = function(timers, chunkNumber, resourceId) {
    return _.find(timers, function(timer) {
      return (timer.getChunkNumber() === chunkNumber) && (timer.getResource().getId() === resourceId);
    });
  }

  return ResponseTimer;

});
