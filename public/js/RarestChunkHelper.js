define('RarestChunkHelper', [], function () {

  'use strict';

  var RarestChunkHelper = RarestChunkHelper || {};

  var initStructure = function (peerBitfield) {
    return _.reduce(peerBitfield, function(memo, chunk, index) {
      memo[index] = (chunk === 1) ? undefined : { count: 0, peer: undefined, chunkNumber: index };
      return memo;
    }, []);
  }

  var processBitfields = function (structure, peers) {
    return _.reduce(peers, function(memo, peer){
      return processBitfield(memo, peer);
    }, structure);
  }

  var processBitfield = function(structure, peer) {
    _.each(peer.bitfield, function(chunk, index) {
      if ((structure[index] !== undefined) && (chunk === 1)) {
        structure[index].count += 1;
        structure[index].peer = peer;
      }
    });

    return structure;
  }

  var selectExistingChunks = function (structure) {
    return _.select(structure, function(chunk) { return (chunk !== undefined) && (chunk.count !== 0) });
  }

  var selectUnexistingChunks = function (structure) {
    return _.select(structure, function(chunk) { return (chunk !== undefined) && (chunk.count === 0) });
  }

  var sortByRareness = function (structure) {
    return _.sortBy(structure, function(chunk) { return chunk.count });
  }

  var ponderStructure = function (structure) {
    var lelftHalf = structure.splice(0, structure.length / 2);
    var rightHalf = structure;
    return lelftHalf.concat(lelftHalf).concat(lelftHalf).concat(rightHalf);
  }

  RarestChunkHelper.findRarestChunk = function (hostBitfield, peers) {
    var structure;

    structure = initStructure(hostBitfield);
    structure = processBitfields(structure, peers);
    structure = selectExistingChunks(structure);
    structure = sortByRareness(structure);

    return structure[0];
  }

  RarestChunkHelper.findRandomUnexistingChunk = function (hostBitfield, peers) {
    var structure;

    structure = initStructure(hostBitfield);
    structure = processBitfields(structure, peers);
    structure = selectUnexistingChunks(structure);
    structure = ponderStructure(structure);

    return structure[_.random(structure.length - 1)];
  }

  return RarestChunkHelper;

});
