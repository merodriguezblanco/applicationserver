define('Receiver', ['MediaSourceProxy', 'MediaSourceWrapper', 'ResponseTimer', 'HostPeer'],
  function (MediaSourceProxy, MediaSourceWrapper, ResponseTimer, HostPeer) {

  'use strict';

  var timers = [];

  var initialize = function () {
    globalEventListener.addEventListener('documentOnChunkRequested', onChunkRequested);
    globalEventListener.addEventListener('documentOnChunkReceived', onChunkReceived);
    globalEventListener.addEventListener('documentOnChunkEmpty', onChunkEmpty);
    globalEventListener.addEventListener('documentOnChunkFailed', onChunkFailed);
  }

  var onChunkRequested = function (event) {
    var data    = event.data,
        resource = HostPeer.getResource(data.resourceId, data.resourceType);
    startTimer(data.number, resource);
  }

  var onChunkReceived = function(event) {
    var data     = event.data,
        resource = HostPeer.getResource(data.resourceId, data.resourceType);

    MediaSourceProxy.loadChunk(data.chunkNumber, data.binary, resource);
    stopTimer(data.chunkNumber, resource.getId());
    HostPeer.evaluateUninterestForPeers(data.chunkNumber);
    HostPeer.sendHaveToPeers(data.chunkNumber);

    if (MediaSourceProxy.isFileCompleted()) {
      MediaSourceWrapper.closeStream();
    }
  }

  var onChunkEmpty = function(event) {
    var data = event.data;

    MediaSourceProxy.markChunkAsNotRequested(data.chunkNumber);
    stopTimer(data.chunkNumber, data.resourceId);
  }

  var onChunkFailed = function(event) {
    var data = event.data;

    MediaSourceProxy.markChunkAsNotRequested(data.chunkNumber);
  }

  var startTimer = function(chunkNumber, resource) {
    var timer = new ResponseTimer(chunkNumber, resource);
    timers.push(timer);
  }

  var stopTimer = function(chunkNumber, resourceId) {
    var timer = ResponseTimer.find(timers, chunkNumber, resourceId);

    if (timer) { // TODO take this out once timers are used for peers as well
      timer.stopTimer();
      timers = _.without(timers, timer);
    }
  }

  return {
    initialize: initialize
  }

});
