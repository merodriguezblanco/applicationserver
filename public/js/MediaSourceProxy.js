define('MediaSourceProxy', ['MediaChunk', 'VideoWrapper', 'MediaSourceWrapper', 'TorrentParser', 'Front', 'Tracker'],
       function (MediaChunk, VideoWrapper, MediaSourceWrapper, TorrentParser, Front, Tracker) {

  'use strict';

  var URGENT_TIME_LIMIT = 30,  // seconds
      NORMAL_TIME_LIMIT = 50; // seconds

  var mediaChunks = [];
  var duration;
  var lastBuffered = -1;

  var initialize = function() {
    var metaData = TorrentParser.parseVideoMetadata();
    duration = metaData.duration;

    mediaChunks[0] = new MediaChunk(metaData.init);
    _.each(metaData.media, function(media, index) {
      mediaChunks[index + 1] = new MediaChunk(metaData.media[index]);
    });

    MediaSourceWrapper.initialize(metaData);
    setTimeout(Front.renderChunkBar(mediaChunks.length), 0);
  }

  var markChunkAsRequested = function(index) {
    mediaChunks[index].markAsRequested();
  }

  var markChunkAsNotRequested = function(index) {
    mediaChunks[index].markAsNotRequested();
  }

  var loadChunk = function(index, chunk, resource) {
    if (mediaChunks[index].isLoaded()) {
      setTimeout(Front.animateChunkAsDiscarded(index), 0);
    } else {
      mediaChunks[index].setChunk(chunk);
      setTimeout(Front.incrementTopic(resource.getId()), 0);
      setTimeout(Front.paintChunkAsLoaded(index, resource.getId()), 0);
      resource.updateStatsOnChunkLoaded(chunk);

      appendAllConsecutiveChunks();
    }
  }

  var appendAllConsecutiveChunks = function() {
    for (var index = lastBuffered + 1;
         !isFileCompleted() && mediaChunks[index].isLoaded();
         index++) {
      appendChunkToBuffer(index);
    }
  }

  var appendChunkToBuffer = function(index) {
    var mediaChunk = mediaChunks[index];

    MediaSourceWrapper.queueInBuffer(mediaChunk.getChunk());
    mediaChunk.markAsBuffered();
    lastBuffered = index;

    setTimeout(Front.paintChunkAsBuffered(index), 0);
  }

  var getChunk = function(index) {
    return mediaChunks[index].getChunk();
  }

  var getExecutionChunkNumber = function() {
    var time = VideoWrapper.getExecutionTime();

    //TODO: PLEEEEASE OPTIMIZE!!!!!
    var i = 1;
    if (time > 0) {
      while (mediaChunks[i] !== undefined &&
             mediaChunks[i].getTime() <= time) {
        i++;
      }
    }

    return i - 1;
  }

  var getNextChunkNumber = function () {
    var i = 0;
    var mediaChunk = mediaChunks[i];

    while (!_.isUndefined(mediaChunk) &&
           !mediaChunk.isRequestable()) {
      i++;
      mediaChunk = mediaChunks[i];
    }

    return (_.isUndefined(mediaChunk) || !isChunkUrgent(mediaChunk) ? undefined : i);
  }

  var isChunkUrgent = function (mediaChunk) {
    if (lastBuffered > 0) {
      return (mediaChunk.getTime() - mediaChunks[lastBuffered].getTime() < URGENT_TIME_LIMIT);
    } else {
      return true
    }
  }

  var getCDNRequestData = function(index) {
    return {
      number: index,
      offset: mediaChunks[index].getOffset(),
      size: mediaChunks[index].getSize()
    }
  }

  var getBitfield = function () {
    var result = [];

    _.each(mediaChunks, function(chunk, index) {
      if(_.isUndefined(mediaChunks[index].getChunk())) {
        result[index] = 0;
      } else {
        result[index] = 1;
      }
    })

    return result;
  }

  var hasNeededChunks = function (bitfield, exceptionIndex) {
    var ownBitfield = getBitfield();
    var index = lastBuffered;

    while (index < mediaChunks.length) {
      if ((index !== exceptionIndex) &&
          (bitfield[index] && !ownBitfield[index])) {
        return true;
      }
      index++;
    }

    return false;
  }

  var isFileCompleted = function() {
    return (lastBuffered === mediaChunks.length - 1);
  }

  var lastBufferedTime = function () {
    var lastBufferedMediaChunk = mediaChunks[lastBuffered];

    if (_.isUndefined(lastBufferedMediaChunk)) {
      return 0;
    } else {
      return lastBufferedMediaChunk.getTime() || 0;
    }
  }

  var bufferedTime = function() {
    return lastBufferedTime() - VideoWrapper.getExecutionTime();
  }

  var isUrgentWindow = function () {
    return bufferedTime() <= URGENT_TIME_LIMIT;
  }

  var isNormalWindow = function () {
    return (bufferedTime() > URGENT_TIME_LIMIT) && (bufferedTime() <= NORMAL_TIME_LIMIT);
  }

  var isFutureWindow = function () {
    return bufferedTime() > NORMAL_TIME_LIMIT;
  }

  var isReproductionEndend = function () {
    return VideoWrapper.isEnded();
  }

  return {
    initialize: initialize,
    markChunkAsRequested: markChunkAsRequested,
    markChunkAsNotRequested: markChunkAsNotRequested, 

    loadChunk: loadChunk,

    getChunk: getChunk,
    getExecutionChunkNumber: getExecutionChunkNumber,
    getNextChunkNumber: getNextChunkNumber,
    getCDNRequestData: getCDNRequestData,
    getBitfield: getBitfield,
    hasNeededChunks: hasNeededChunks,

    isFileCompleted: isFileCompleted,
    isUrgentWindow: isUrgentWindow,
    isNormalWindow: isNormalWindow,
    isFutureWindow: isFutureWindow,
    isReproductionEndend: isReproductionEndend
  }

});
