var express  = require('express'),
    http     = require('http'),
    socketio = require('socket.io');

var serverPort = process.env.PORT || process.argv.pop();
serverPort = isNaN(serverPort) ? 5000 : serverPort;

var app = express();
var server = http.createServer(app);
var assetsRoute = 'development';

app.configure('production', function () {
  assetsRoute = '/dist';
});

app.configure('development', function () {
  assetsRoute = '/public';
});

app.set('view engine', 'ejs');
app.use(express.static(__dirname + assetsRoute));

app.get('/', function (request, response) {
  var route = '..' + assetsRoute + '/index';

  response.render(route, { frontEnabled: request.param('front') })
});

server.listen(serverPort);
console.log('App Server listening on port ' + serverPort);

socketio.listen(server);
